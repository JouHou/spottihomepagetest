/*!
* Start Bootstrap - Scrolling Nav v5.0.4 (https://startbootstrap.com/template/scrolling-nav)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-scrolling-nav/blob/master/LICENSE)
*/
//
// Scripts
// 

const roundAccurately = (number, decimalPlaces) => Number(Math.round(number + 'e' + decimalPlaces) + 'e-' + decimalPlaces);

window.addEventListener('DOMContentLoaded', event => {

    // Activate Bootstrap scrollspy on the main nav element
    const mainNav = document.body.querySelector('#mainNav');
    if (mainNav) {
        new bootstrap.ScrollSpy(document.body, {
            target: '#mainNav',
            offset: 74,
        });
    };

    // Collapse responsive navbar when toggler is visible
    const navbarToggler = document.body.querySelector('.navbar-toggler');
    const responsiveNavItems = [].slice.call(
        document.querySelectorAll('#navbarResponsive .nav-link')
    );
    responsiveNavItems.map(function (responsiveNavItem) {
        responsiveNavItem.addEventListener('click', () => {
            if (window.getComputedStyle(navbarToggler).display !== 'none') {
                navbarToggler.click();
            }
        });
    });

});

const date = new Date();
const tomorrow = new Date();
tomorrow.setDate(tomorrow.getDate() + 1);

fetch(`https://spottiappi.herokuapp.com/api/v2/prices/fi/${date.getDate()}_${date.getMonth() + 1}_${date.getFullYear()}`)
.then((val) => {return val.json()})
.then((content) => { 

  var avgPriceToday = roundAccurately(content.prices.prices.reduce((sum, value) => { return sum + value;}, 0) / content.prices.prices.length, 2);

  const todayHtml = `
    <div style="margin: 2rem; display: block; margin-left: auto;">
      <h2>Tänään</h2>
      <table style="margin-top: 1.5rem;">
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">Kellonaika</td>
          <td style="font-weight: bold; padding-right: 1rem;">snt/kWh</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">00</td>
          <td>${content.prices.prices[0].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">01</td>
          <td>${content.prices.prices[1].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">02</td>
          <td>${content.prices.prices[2].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">03</td>
          <td>${content.prices.prices[3].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">04</td>
          <td>${content.prices.prices[4].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">05</td>
          <td>${content.prices.prices[5].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">06</td>
          <td>${content.prices.prices[6].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">07</td>
          <td>${content.prices.prices[7].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">08</td>
          <td>${content.prices.prices[8].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">09</td>
          <td>${content.prices.prices[9].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">10</td>
          <td>${content.prices.prices[10].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">11</td>
          <td>${content.prices.prices[11].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">12</td>
          <td>${content.prices.prices[12].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">13</td>
          <td>${content.prices.prices[13].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">14</td>
          <td>${content.prices.prices[14].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">15</td>
          <td>${content.prices.prices[15].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">16</td>
          <td>${content.prices.prices[16].toFixed(2)}</td>
        </tr> 
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">17</td>
          <td>${content.prices.prices[17].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">18</td>
          <td>${content.prices.prices[18].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">19</td>
          <td>${content.prices.prices[19].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">20</td>
          <td>${content.prices.prices[20].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">21</td>
          <td>${content.prices.prices[21].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">22</td>
          <td>${content.prices.prices[22].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">23</td>
          <td>${content.prices.prices[23].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;"><hr/></td>
          <td></td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">Korkein</td>
          <td>${Math.max(...content.prices.prices).toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">Matalin</td>
          <td>${Math.min(...content.prices.prices).toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">Keskiarvo</td>
          <td>${avgPriceToday}</td>
        </tr>
      </table> 
    </div>
  `

  document.getElementById("pricesTableToday").innerHTML = todayHtml;
}); 

fetch(`https://spottiappi.herokuapp.com/api/v2/prices/fi/${tomorrow.getDate()}_${tomorrow.getMonth() + 1}_${tomorrow.getFullYear()}`)
.then((val) => {return val.json()})
.then((content) => { 

  var avgPriceToday = roundAccurately(content.prices.prices.reduce((sum, value) => { return sum + value;}, 0) / content.prices.prices.length, 2);

  const todayHtml = `
    <div style="margin: 2rem; display: block; margin-left: auto;">
      <h2>Huomenna</h2>
      <table style="margin-top: 1.5rem;">
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">Kellonaika</td>
          <td style="font-weight: bold; padding-right: 1rem;">snt/kWh</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">00</td>
          <td>${content.prices.prices[0].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">01</td>
          <td>${content.prices.prices[1].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">02</td>
          <td>${content.prices.prices[2].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">03</td>
          <td>${content.prices.prices[3].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">04</td>
          <td>${content.prices.prices[4].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">05</td>
          <td>${content.prices.prices[5].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">06</td>
          <td>${content.prices.prices[6].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">07</td>
          <td>${content.prices.prices[7].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">08</td>
          <td>${content.prices.prices[8].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">09</td>
          <td>${content.prices.prices[9].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">10</td>
          <td>${content.prices.prices[10].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">11</td>
          <td>${content.prices.prices[11].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">12</td>
          <td>${content.prices.prices[12].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">13</td>
          <td>${content.prices.prices[13].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">14</td>
          <td>${content.prices.prices[14].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">15</td>
          <td>${content.prices.prices[15].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">16</td>
          <td>${content.prices.prices[16].toFixed(2)}</td>
        </tr> 
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">17</td>
          <td>${content.prices.prices[17].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">18</td>
          <td>${content.prices.prices[18].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">19</td>
          <td>${content.prices.prices[19].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">20</td>
          <td>${content.prices.prices[20].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">21</td>
          <td>${content.prices.prices[21].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">22</td>
          <td>${content.prices.prices[22].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">23</td>
          <td>${content.prices.prices[23].toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;"><hr/></td>
          <td></td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">Korkein</td>
          <td>${Math.max(...content.prices.prices).toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">Pienin</td>
          <td>${Math.min(...content.prices.prices).toFixed(2)}</td>
        </tr>
        <tr style="margin: 2rem">
          <td style="font-weight: bold; padding-right: 1rem;">Keskiarvo</td>
          <td>${avgPriceToday}</td>
        </tr>
      </table> 
    </div>
  `

  document.getElementById("pricesTableTomorrow").innerHTML = todayHtml;
}); 


// Promise.all([
//   fetch(`https://spotticorstest.herokuapp.com/api/v2/prices/fi/${date.getDate()}_${date.getMonth() + 1}_${date.getFullYear()}`).then(val => val.json()),
//   fetch(`https://spotticorstest.herokuapp.com/api/v2/prices/fi/${tomorrow.getDate()}_${tomorrow.getMonth() + 1}_${tomorrow.getFullYear()}`).then(val => val.json())
// ])
// .then(([todayResult, tomorrowResult]) => { return { today: todayResult, tomorrow: tomorrowResult }; })
// .then((content) => { 

//   var avgPriceToday = roundAccurately(content.today.prices.prices.reduce((sum, value) => { return sum + value;}, 0) / content.today.prices.prices.length, 2);
//   var avgPriceTomorrow = roundAccurately(content.tomorrow.prices.prices.reduce((sum, value) => { return sum + value;}, 0) / content.tomorrow.prices.prices.length, 2);

//   const todayHtml = `
//     <div style="margin: 3rem; display: block; margin-left: auto;">
//       <h2>Tänään</h2>
//       <table style="margin-top: 1.5rem;">
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">Kellonaika</td>
//           <td>snt/kWh</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">00</td>
//           <td>${content.today.prices.prices[0].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">01</td>
//           <td>${content.today.prices.prices[1].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">02</td>
//           <td>${content.today.prices.prices[2].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">03</td>
//           <td>${content.today.prices.prices[3].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">04</td>
//           <td>${content.today.prices.prices[4].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">05</td>
//           <td>${content.today.prices.prices[5].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">06</td>
//           <td>${content.today.prices.prices[6].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">07</td>
//           <td>${content.today.prices.prices[7].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">08</td>
//           <td>${content.today.prices.prices[8].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">09</td>
//           <td>${content.today.prices.prices[9].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">10</td>
//           <td>${content.today.prices.prices[10].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">11</td>
//           <td>${content.today.prices.prices[11].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">12</td>
//           <td>${content.today.prices.prices[12].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">13</td>
//           <td>${content.today.prices.prices[13].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">14</td>
//           <td>${content.today.prices.prices[14].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">15</td>
//           <td>${content.today.prices.prices[15].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">16</td>
//           <td>${content.today.prices.prices[16].toFixed(2)}</td>
//         </tr> 
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">17</td>
//           <td>${content.today.prices.prices[17].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">18</td>
//           <td>${content.today.prices.prices[18].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">19</td>
//           <td>${content.today.prices.prices[19].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">20</td>
//           <td>${content.today.prices.prices[20].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">21</td>
//           <td>${content.today.prices.prices[21].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">22</td>
//           <td>${content.today.prices.prices[22].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">23</td>
//           <td>${content.today.prices.prices[23].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;"><hr/></td>
//           <td></td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">Maks.</td>
//           <td>${Math.max(...content.today.prices.prices).toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">Min.</td>
//           <td>${Math.min(...content.today.prices.prices).toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">Ka.</td>
//           <td>${avgPriceToday}</td>
//         </tr>
//       </table> 
//     </div>
//   `

//   const tomorrowHtml = `
//     <div style="margin: 3rem; display: block; margin-right: auto;">
//       <h2>Huomenna</h2>
//       <table style="margin-top: 1.5rem;">
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">Kellonaika</td>
//           <td>snt/kWh</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">00</td>
//           <td>${content.tomorrow.prices.prices[0].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">01</td>
//           <td>${content.tomorrow.prices.prices[1].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">02</td>
//           <td>${content.tomorrow.prices.prices[2].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">03</td>
//           <td>${content.tomorrow.prices.prices[3].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">04</td>
//           <td>${content.tomorrow.prices.prices[4].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">05</td>
//           <td>${content.tomorrow.prices.prices[5].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">06</td>
//           <td>${content.tomorrow.prices.prices[6].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">07</td>
//           <td>${content.tomorrow.prices.prices[7].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">08</td>
//           <td>${content.tomorrow.prices.prices[8].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">09</td>
//           <td>${content.tomorrow.prices.prices[9].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">10</td>
//           <td>${content.tomorrow.prices.prices[10].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">11</td>
//           <td>${content.tomorrow.prices.prices[11].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">12</td>
//           <td>${content.tomorrow.prices.prices[12].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">13</td>
//           <td>${content.tomorrow.prices.prices[13].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">14</td>
//           <td>${content.tomorrow.prices.prices[14].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">15</td>
//           <td>${content.tomorrow.prices.prices[15].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">16</td>
//           <td>${content.tomorrow.prices.prices[16].toFixed(2)}</td>
//         </tr> 
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">17</td>
//           <td>${content.tomorrow.prices.prices[17].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">18</td>
//           <td>${content.tomorrow.prices.prices[18].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">19</td>
//           <td>${content.tomorrow.prices.prices[19].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">20</td>
//           <td>${content.tomorrow.prices.prices[20].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">21</td>
//           <td>${content.tomorrow.prices.prices[21].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">22</td>
//           <td>${content.tomorrow.prices.prices[22].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">23</td>
//           <td>${content.tomorrow.prices.prices[23].toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;"><hr/></td>
//           <td></td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">Maks.</td>
//           <td>${Math.max(...content.tomorrow.prices.prices).toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">Min.</td>
//           <td>${Math.min(...content.tomorrow.prices.prices).toFixed(2)}</td>
//         </tr>
//         <tr style="margin: 2rem">
//           <td style="font-weight: bold; padding-right: 1rem;">Ka.</td>
//           <td>${avgPriceTomorrow}</td>
//         </tr>
//       </table> 
//     </div>
//   `

//   const bothDaysHtml = `
//   <div style="display: flex; flex-direction: row; align-content: center;">
//     ${content.today.prices.prices ? todayHtml : "<h2>Ei tämän päivän hintoja saatavilla</h2>"}
//     ${content.tomorrow.prices.prices ? tomorrowHtml : "<h2>Ei huomisen päivän hintoja saatavilla</h2>"}
//   </div>
//   `

//   document.getElementById("pricesTables").innerHTML = bothDaysHtml;

//   // document.getElementById("pricesTables").innerHTML = `
//   // <div style="display: flex; flex-direction: row; align-content: center;">
//   //   <div style="margin: 3rem; display: block; margin-left: auto;">
//   //     <h2>Tänään</h2>
//   //     <table style="margin: 2rem">
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">Kellonaika</td>
//   //         <td>snt/kWh</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">00</td>
//   //         <td>${content.today.prices.prices[0].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">01</td>
//   //         <td>${content.today.prices.prices[1].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">02</td>
//   //         <td>${content.today.prices.prices[2].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">03</td>
//   //         <td>${content.today.prices.prices[3].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">04</td>
//   //         <td>${content.today.prices.prices[4].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">05</td>
//   //         <td>${content.today.prices.prices[5].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">06</td>
//   //         <td>${content.today.prices.prices[6].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">07</td>
//   //         <td>${content.today.prices.prices[7].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">08</td>
//   //         <td>${content.today.prices.prices[8].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">09</td>
//   //         <td>${content.today.prices.prices[9].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">10</td>
//   //         <td>${content.today.prices.prices[10].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">11</td>
//   //         <td>${content.today.prices.prices[11].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">12</td>
//   //         <td>${content.today.prices.prices[12].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">13</td>
//   //         <td>${content.today.prices.prices[13].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">14</td>
//   //         <td>${content.today.prices.prices[14].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">15</td>
//   //         <td>${content.today.prices.prices[15].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">16</td>
//   //         <td>${content.today.prices.prices[16].toFixed(2)}</td>
//   //       </tr> 
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">17</td>
//   //         <td>${content.today.prices.prices[17].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">18</td>
//   //         <td>${content.today.prices.prices[18].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">19</td>
//   //         <td>${content.today.prices.prices[19].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">20</td>
//   //         <td>${content.today.prices.prices[20].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">21</td>
//   //         <td>${content.today.prices.prices[21].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">22</td>
//   //         <td>${content.today.prices.prices[22].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">23</td>
//   //         <td>${content.today.prices.prices[23].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;"><hr/></td>
//   //         <td></td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">Maks.</td>
//   //         <td>${Math.max(...content.today.prices.prices).toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">Min.</td>
//   //         <td>${Math.min(...content.today.prices.prices).toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">Ka.</td>
//   //         <td>${avgPriceToday}</td>
//   //       </tr>
//   //     </table> 
//   //   </div>
//   //   <div style="margin: 3rem; display: block; margin-right: auto;">
//   //     <h2>Huomenna</h2>
//   //     <table style="margin: 2rem;">
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">Kellonaika</td>
//   //         <td>snt/kWh</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">00</td>
//   //         <td>${content.tomorrow.prices.prices[0].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">01</td>
//   //         <td>${content.tomorrow.prices.prices[1].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">02</td>
//   //         <td>${content.tomorrow.prices.prices[2].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">03</td>
//   //         <td>${content.tomorrow.prices.prices[3].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">04</td>
//   //         <td>${content.tomorrow.prices.prices[4].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">05</td>
//   //         <td>${content.tomorrow.prices.prices[5].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">06</td>
//   //         <td>${content.tomorrow.prices.prices[6].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">07</td>
//   //         <td>${content.tomorrow.prices.prices[7].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">08</td>
//   //         <td>${content.tomorrow.prices.prices[8].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">09</td>
//   //         <td>${content.tomorrow.prices.prices[9].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">10</td>
//   //         <td>${content.tomorrow.prices.prices[10].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">11</td>
//   //         <td>${content.tomorrow.prices.prices[11].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">12</td>
//   //         <td>${content.tomorrow.prices.prices[12].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">13</td>
//   //         <td>${content.tomorrow.prices.prices[13].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">14</td>
//   //         <td>${content.tomorrow.prices.prices[14].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">15</td>
//   //         <td>${content.tomorrow.prices.prices[15].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">16</td>
//   //         <td>${content.tomorrow.prices.prices[16].toFixed(2)}</td>
//   //       </tr> 
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">17</td>
//   //         <td>${content.tomorrow.prices.prices[17].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">18</td>
//   //         <td>${content.tomorrow.prices.prices[18].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">19</td>
//   //         <td>${content.tomorrow.prices.prices[19].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">20</td>
//   //         <td>${content.tomorrow.prices.prices[20].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">21</td>
//   //         <td>${content.tomorrow.prices.prices[21].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">22</td>
//   //         <td>${content.tomorrow.prices.prices[22].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">23</td>
//   //         <td>${content.tomorrow.prices.prices[23].toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;"><hr/></td>
//   //         <td></td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">Maks.</td>
//   //         <td>${Math.max(...content.tomorrow.prices.prices).toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">Min.</td>
//   //         <td>${Math.min(...content.tomorrow.prices.prices).toFixed(2)}</td>
//   //       </tr>
//   //       <tr style="margin: 2rem">
//   //         <td style="font-weight: bold; padding-right: 1rem;">Ka.</td>
//   //         <td>${avgPriceTomorrow}</td>
//   //       </tr>
//   //     </table> 
//   //   </div>
//   // </div>
//   // `
// }); 
